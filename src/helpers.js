export function formatPrice(cops) {
  return (
    (cops / 100).toLocaleString("ru-RU", {
      style : "currency",
      currency : "RUB"
    })
  )

}