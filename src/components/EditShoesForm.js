import React from 'react';

class EditShoesForm extends React.Component {
  handleChange = (event) => {
    const updatedShoes = {
      ...this.props.shoes,
      [event.currentTarget.name] : event.currentTarget.value
    };
    this.props.updatedShoes(this.props.index, updatedShoes);
  }
  deleteOnClick = () => {
    this.props.deleteShoes(this.props.index)
  }
  render() {
    return (
      <form className="editShoesForm" onSubmit={this.createShoes}>

        <input className="editShoesForm__input editShoesForm__input--name " 
          name="name"
          value={this.props.shoes.name}  
          type="text" 
          onChange={this.handleChange}          
        />

        <input className="editShoesForm__input editShoesForm__input--price "
          name="price"
          value={this.props.shoes.price} 
          type="text" 
          onChange={this.handleChange}      
         />

        <select name="status"
        value={this.props.shoes.status} 
        onChange={this.handleChange}
         className="editShoesForm__input editShoesForm__input--status">
          <option value="available">Ready to Order</option>
          <option value="unavailable">Sold Out</option>
        </select>

        <input className="editShoesForm__input editShoesForm__input--image "
          name="image"
          value={this.props.shoes.image} 
          type="text" 
          onChange={this.handleChange}
         />

        <textarea className="editShoesForm__input editShoesForm__input--description "
          name="description"
          value={this.props.shoes.description} 
          type="text" 
          onChange={this.handleChange}
          >
         </textarea>   

         <button onClick={this.deleteOnClick}> Remove shoes</button>

      </form>
    );

  }
}
export default EditShoesForm;