 import React from 'react';
 import "./AddShoesForm.css";

 class AddShoesForm extends React.Component {
   nameRef = React.createRef();
   priceRef = React.createRef();
   statusRef = React.createRef();
   imageRef = React.createRef();
   descriptionRef = React.createRef();

  createShoes = (event) =>  {
    event.preventDefault();
    const shoes = {
      name: this.nameRef.current.value,
      price: parseFloat(this.priceRef.current.value),
      status : this.statusRef.current.value,
      image: this.imageRef.current.value,
      description : this.descriptionRef.current.value,
    }
    this.props.addShoes(shoes);
    event.currentTarget.reset();
       
  }
  
  render() {
    return (
      <form className="addShoesForm" onSubmit={this.createShoes}>

        <input className="addShoesForm__input addShoesForm__input--name " 
          name="name"  
          ref = {this.nameRef}
          type="text" 
          placeholder="name"
        />

        <input className="addShoesForm__input addShoesForm__input--price "
          name="price" 
          ref = {this.priceRef}
          type="text" 
          placeholder="price"
         />

        <select name="status" 
        ref = {this.statusRef} className="addShoesForm__input addShoesForm__input--status">
          <option value="available">Ready to Order</option>
          <option value="unavailable">Sold Out</option>
        </select>

        <input className="addShoesForm__input addShoesForm__input--image "
          name="image" 
          ref = {this.imageRef}
          type="text" 
          placeholder="image"
         />

        <textarea className="addShoesForm__input addShoesForm__input--description "
          name="description" 
          ref = {this.descriptionRef}
          type="text" 
          placeholder="description">
         </textarea>

        

         <button 
          className="addShoesForm__button"
          type="submit"> Add Shoes
         
         </button>

      </form>
    );
  }
 }

 export default AddShoesForm;