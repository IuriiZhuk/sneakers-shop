import React from 'react';
import AddShoesForm from './AddShoesForm';
import EditShoesForm from './EditShoesForm';

class ListItems extends React.Component {
  render() {
    return (
      <section className="listItems">
        <h2> Shoes List</h2>
        {Object.keys(this.props.shoes).map( key => <EditShoesForm index={key} key ={key} deleteShoes={this.props.deleteShoes} updatedShoes={this.props.updatedShoes} shoes={this.props.shoes[key] }/> )}
        <AddShoesForm addShoes={this.props.addShoes} />
        <button onClick={this.props.loadTemplateShoes}> ADD Template Shoes</button>
      </section>
    );
  }

}

export default ListItems;
