import React from 'react';
import {formatPrice} from '../helpers';


class Cart extends React.Component {
  renderOrder = (key) => {
    const shoes = this.props.shoes[key];
    const count = this.props.order[key];
    const isAvailable = shoes && shoes.status === "available";

    
    if(!shoes){return (
           null
      )
    };

    if (!isAvailable) {

     return (
      <li key={key} className="cart__item"> Sorry, {shoes ? shoes.name : "sneacker" } isn't available</li>
     )
    }
    return(
      <li key={key} className="cart__item"> 
       {count} шт. {shoes.name} ---- {formatPrice(count * shoes.price)} 
       <button className="cart__button" onClick={ () => this.props.removeFromOrderTotally(key)}> Delete </button>
       </li>
      )
    
  }

  render() {
    const ordersIds = Object.keys(this.props.order);
    const total = ordersIds.reduce((previous, key) => {
      const shoes = this.props.shoes[key];
      const count = this.props.order[key];
      const isAvailable = shoes && shoes.status === "available";
     
      
      if (isAvailable) {
        
        return previous + (count * shoes.price);
      }
      return previous;
    }, 0);
    return (
      <section className="cart">
        <h2 className="cart__title"> Your Order</h2>
        
          {ordersIds.map(this.renderOrder)}
        
        <div className="cart__summ">
          Summ : {formatPrice(total)}
        </div>

      </section>
    );
  }

}

export default Cart;
