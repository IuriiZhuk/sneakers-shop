import React from 'react';

class Headpiece extends React.Component {
  goToStore = (event) => {
    event.preventDefault();
    this.props.history.push(`/store`);
  }
  render() {
    return (
      <form  className="headpiece" onSubmit ={this.goToStore}>
        <h1 className="headpiece__title">Welcome </h1>
        <button type="submit" className="headpiece__button">GO ->>></button>
      </form>
    );
  }
}


export default Headpiece;