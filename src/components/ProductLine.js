import React from 'react';
import Shoes from './Shoes';

class ProductLine extends React.Component {
  render() {
    return (
      <section className="product-line">
      <h2 className="product-line__title">Choose a sneaker</h2>
        <ul className="shoes__list">
          {Object.keys(this.props.shoesData)
            .map( key => <Shoes key={key}
                          index={key}
                          details={this.props.shoesData[key]}
                          addToOrder={this.props.addToOrder}
                          removeFromOrder={this.props.removeFromOrder} 
                        />)}
        </ul>
      </section>
    );
  }

}

export default ProductLine;
