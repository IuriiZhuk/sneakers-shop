import React from 'react';
import {formatPrice} from '../helpers';

class Shoes extends React.Component {
  handleClick = () => {
    this.props.addToOrder(this.props.index)
  }
  removeOrder =() => {
    this.props.removeFromOrder(this.props.index)
  }
  render() {
    const {description,image,name,price,status} = this.props.details;
    const isAvailable = status === "available";
    return (
      <li className="shoes__item">
        <div className="shoes__image-wrapper">
          <img  className="shoes__image" src={image} alt={name}/>
        </div>
        <div className="shoes__about">
          <h3 className="shoes__name">{name} <span className="shoes__price">{formatPrice(price)}</span></h3>
          <p className="shoes__description">{description}</p>
          <button className="shoes__button" disabled={!isAvailable} onClick={this.handleClick}>{isAvailable ? "Add to Cart" : "Sorry , sold out"}</button>
          <button onClick={this.removeOrder} disabled={!isAvailable}>Remove from order</button>
        </div>        
       
      </li>
    );
  }

}

export default Shoes;