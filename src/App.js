import React,{Fragment} from 'react';
import ProductLine from './components/ProductLine';
import Cart from './components/Cart';
import ListItems from './components/ListItems';

import './App.css';
import shoesTemplate from './templateShoes';
import base from './base';

class App extends React.Component {
  state = {
    shoes: {},
    order: {}
  };

  componentDidMount() {
    const localStorageRef = localStorage.getItem(this.props.match.path);
    if (localStorageRef) {
      
      this.setState({order: JSON.parse(localStorageRef)})
    }
    this.ref = base.syncState(`${this.props.match.path}`,{
      context: this,
      state: "shoes"
    })
  }

  componentWillUnmount() {
    base.removeBinding(this.ref);
  }

  componentDidUpdate() {
    
    localStorage.setItem(this.props.match.path, JSON.stringify(this.state.order));
  }

  addShoes = (shoes) => {
    const shoesState = {...this.state.shoes};
    shoesState[`shoes${Date.now()}`] = shoes;
    this.setState({
      shoes: shoesState
    })
  }

  deleteShoes = (key) => {
    const shoes = {...this.state.shoes};
    shoes[key] = null;
    this.setState({shoes})
  }
  
  loadTemplateShoes = ()=> {
    this.setState({
      shoes : shoesTemplate
    });
  }

  addToOrder = (key) => {
    const order = {...this.state.order};
    order[key] = order[key] + 1 || 1;
    this.setState({order});
  }
  removeFromOrder = (key) => {
    const order = {...this.state.order};
    if(order[key]) {
      order[key] = order[key] - 1 || 0;
      this.setState({order});
    }
    else this.removeFromOrderTotally(key);
    
  }

  updatedShoes = (key, updatedShoes) => {
    const shoes  = {...this.state.shoes};
    shoes[key] = updatedShoes;
    this.setState({shoes})
  }

  removeFromOrderTotally = (key) => {
    const order = {...this.state.order};
    delete order[key];
    this.setState({order});


  }


  render() {
    return (
        <Fragment>
          <header className="main-header">
            <h2 className="main-header__title">Main-title</h2>
          </header>

          <main className="main-content">
            <ProductLine shoesData = {this.state.shoes} addToOrder={this.addToOrder} removeFromOrder={this.removeFromOrder}/>
            <Cart shoes = {this.state.shoes} order={this.state.order} removeFromOrderTotally={this.removeFromOrderTotally}/>
            <ListItems addShoes={this.addShoes} deleteShoes={this.deleteShoes} updatedShoes={this.updatedShoes} loadTemplateShoes = {this.loadTemplateShoes} shoes = {this.state.shoes} />
          </main>

          <footer className="main-footer">
            <h2 className="main-footer__title">Main-footer</h2>
           </footer>
        </Fragment>
    );
  }
}

export default App;
