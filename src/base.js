import Rebase from "re-base";
import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyD6Avx6_xo9OfTn2inM-YSyEqscBv2GStQ",
    authDomain: "sneaker-shop-cdeca.firebaseapp.com",
    databaseURL: "https://sneaker-shop-cdeca.firebaseio.com"
});

const base = Rebase.createClass(firebaseApp.database());

export {firebaseApp};

export default base;