const shoes = {
  shoes1:{
    name:"Nike",
    image:"./images/nike.jpg",
    description: "Lorem ipsum dolor sit amet.",
    price: 10000,
    status: "available"
  },

  shoes2:{
    name:"New Balance",
    image:"./images/nb.jpg",
    description: "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam, asperiores.",
    price: 20000,
    status: "unavailable"
  },

  shoes3:{
    name:"Adidas",
    image:"./images/adidas.jpg",
    description: "Lorem ipsum dolor sit amet.",
    price: 30000,
    status: "available"
  },

  shoes4:{
    name:"Puma",
    image:"./images/puma.jpg",
    description: "Lorem ipsum dolor sit amet, consectetur adipisicing.",
    price: 40000,
    status: "unavailable"
  }
}

export default shoes;