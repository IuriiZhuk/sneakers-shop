import React from 'react';
import ReactDOM from 'react-dom';
import './components/headpiece.css';

import Router from './components/Router';


ReactDOM.render(<Router />, document.getElementById('root'));

